# depcheck.py - core logic and interfaces for checking dependency trees
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Copyright 2013, 2014 John H. Dulaney
# Copyright 2014 Red Hat, Inc
#
# Authors:
#   John H. Dulaney <jdulaney@fedoraproject.org>
#   Tim Flink <tflink@redhat.com>

import logging

# Someone call for a lumberjack?
logger = logging.getLogger('depcheck')
logger.setLevel(logging.INFO)
logger.addHandler(logging.NullHandler())
