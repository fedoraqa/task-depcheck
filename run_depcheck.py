#!/usr/bin/env python

import sys
import os

sys.path.insert(0, os.path.realpath(os.path.dirname(__file__)))

import depcheck
from depcheck import exc
from depcheck import log

def taskotron_run(rpms, arch, repos, report_format, workdir=None, artifactsdir=None):
    """
    rpms: list of rpm files and/or directories containing rpm files
    arch: list of arches that can contain just one 'primary' arch
    repos: dictionary or list of dictionaries
    report_format: "rpms" or "updates"
    """

    # As this is only called from libtaskotron, set the logging
    #  to the same level as libtaskotron.
    libtaskotron_logger = log.logging.getLogger('libtaskotron')
    log.logger.setLevel(libtaskotron_logger.getEffectiveLevel())

    # FIXME: do something saner in the future - nasty fix for the moment
    filtered_arches = [a for a in arch if a not in ['noarch', 'all', 'src']]

    if len(filtered_arches) > 1:
        raise exc.DepcheckException("Too many 'primary' arches provided:"
        " %r" % filtered_arches)
    if len(filtered_arches) == 0:
        raise exc.DepcheckException("No 'primary' arch provided")

    arch = filtered_arches[0].strip()

    repositories = {}

    if isinstance(repos, list):
        for repo in repos:
            for name, url in repo.items():
                while name in repositories:
                    name += "_"
                repositories[name] = url

    elif isinstance(repos, dict):
        repositories = repos

    else:
        raise exc.DepcheckException('Unsupported type of repos arg')


    for name, val in repositories.items():
        if isinstance(val, dict):
            repositories[name] = val[arch]

    return depcheck.run(rpms, arch, repositories, report_format, workdir, artifactsdir)

if __name__ == '__main__':
    depcheck.depcheck_cli()
