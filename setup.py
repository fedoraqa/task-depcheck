from setuptools import setup
from depcheck import __version__

setup(name='depcheckmk2',
      version=__version__,
      description='Automated dependency checker for Fedora',
      url='https://bitbucket.org/fedoraqa/depcheck-mk-2',
      author='John Dulaney',
      author_email='jdulaney@fedoraproject.org',
      license='GPLv2',
      packages=['depcheck'],
      entry_points={
          'console_scripts': [
              'depcheck=depcheck:depcheck_cli'
          ]
      }
)
