# Depcheck Test Suite

Scripts that generate repositories for depcheck testing.

These repos contain specially crafted RPMs, which's combinations should either PASS or FAIL depcheck.

# Running the code

## Running from command line

In order to initiate the script from command line run the command `yaml_to_repos.py $YAML_FILE [$OUTPUT_DIR]`.

The `$YAML_FILE` is a path to the YAML description file (see below), the `$OUTPUT_DIR` specifies the parent directory
in which the repos will be created. If the `$OUTPUT_DIR` is not set, temporary directory is created automatically.

## Running programatically

After importing `yaml_to_repos.py`, run `yaml_to_repos.create_repos($YAML_DATA, [$OUTPUT_DIR])`. The only difference here is,
that you need to pass the parsed yaml file as the first argument, not just a file name.

You can use this snippet to parse a yaml file:

```
import yaml
data = yaml.load(open('my_yaml_file.yaml'))
```

# Creating YAML description files

First of all, you need to create a file describing the 'testcase', the file might look like this:

```
name: Simple happy path
description:
    This is a simple case in which an update does not cause problems
expected_outcome: pass

current_repo:
  -
    envra: foo-0.1-1.fc19.noarch
  -
    envra: moo-0.2-3.fc19.noarch

update_repo:
  -
    envra: bar-0.1-1.fc19.noarch
    requires: foo
```

`name`, `description`, and `expected_outcome` are metadata for the 'testsuite', and are of no significance for the repo creation.

`current_repo`, and `update_repo` are YAML lists of package descriptions. The `current_repo` describes the state of the 'stable' repository,
and the `update_repo` the available update.

Each package is represented by `envra`. To describe dependencies, you can add `requires`, `provides`, `conflicts`, and `obsoletes`.
Thus creating descriptions like these:
```
...

  -
    envra: foo-0.1-1.fc19.noarch
    requires: libbar.so
  -
    envra: bar-0.1-1.fc19.noarch
    provides: libbar.so
    conflicts: moo

...
```
