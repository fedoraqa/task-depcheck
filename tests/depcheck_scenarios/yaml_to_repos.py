# Copyright 2013, Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Author: Josef Skladanka <jskladan@redhat.com>

import os
import yaml
import tempfile
import rpmfluff
import rpmUtils.arch
import rpmUtils.miscutils
import subprocess


__all__ = ['create_repos']

def parseENVRA(envra, fmt = "envra"):
    """
    @param envra string to be 'manipulated' - can be standard style rpm filename,
      envra, envr, nvr
    @param fmt desired format of the string to be returned. Allowed options are:
      envra, envr, nvra, nvr, e, n, v, r, a. If arch is not present in envra,
      'noarch' is returned wherever needed.
    @returns list of strings based on the specified format; if epoch is requested but not
      present, it is se to empty string
    """
    fmt = fmt.lower()
    supported_formats = ['envra', 'envr', 'nvra', 'nvr', 'e', 'n', 'v', 'r', 'a']
    if fmt not in supported_formats:
        raise ValueError("Format '%s' not in supported formats (%s)" % (fmt, ', '.join(supported_formats)))

    # prepare envra so it can be used with rpmUtils.miscutils.splitFilename()
    parts = envra.split('.')
    if parts[-1].lower() != 'rpm':
        parts.append('rpm')
    if parts[-2].lower() not in rpmUtils.arch.arches.keys() + ['noarch', 'src']:
        parts.insert(-1, 'noarch')

    # split the filename
    (n, v, r, e, a) = rpmUtils.miscutils.splitFilename('.'.join(parts))

    if len(fmt) == 1:
        return {'e':e, 'n':n, 'v':v, 'r':r, 'a':a}[fmt]

    return {'envra': (e, n, v, r, a),
            'envr' : (e, n, v ,r),
            'nvra' : (n, v, r ,a),
            'nvr'  : (n, v, r),
           }[fmt]

class MySimpleRpmBuild(rpmfluff.SimpleRpmBuild):
    """Derivative of SimpleRpmBuild which allows setting output directory"""

    def __init__(self, name, version, release, buildArchs=None, basedir = None):
        rpmfluff.SimpleRpmBuild.__init__(self, name, version, release, buildArchs)
        self._base_dir = basedir

    def get_base_dir(self):
        if self._base_dir is None:
            return rpmfluff.SimpleRpmBuild.get_base_dir(self)
        else:
            if not os.path.exists(self._base_dir):
                os.makedirs(self._base_dir)
            return os.path.join(self._base_dir, "test-rpmbuild-%s-%s-%s"%(self.name, self.version, self.release))


def package_to_SimpleRpmBuild(package, rpmdir = None):
    """
    @param package dictionary representing a 'build' parsed from the yaml data
    @param rpmdir path in filesystem specifying destination dir for rpms
    @returns instance of MySimpleRpmBuild
    """

    e,n,v,r,a = parseENVRA(package['envra'])
    p = MySimpleRpmBuild(n, v, r, [a], rpmdir)
    p.epoch = e

    requires = package.get('requires', None)
    provides = package.get('provides', None)
    conflicts = package.get('conflicts', None)
    obsoletes = package.get('obsoletes', None)
    recommends = package.get('recommends', None)
    suggests = package.get('suggests', None)
    supplements = package.get('supplements', None)
    enhances = package.get('enhances', None)

    if requires:
        if isinstance(requires, list):
            for require in requires:
                p.add_requires(require)
        else:
            p.add_requires(requires)
    if recommends:
        if isinstance(recommends, list):
            for recommendation in recommends:
                p.add_recommends(recommendation)
        else:
            p.add_recommends(recommends)
    if suggests:
        if isinstance(suggests, list):
            for suggestion in suggests:
                p.add_suggests(suggestion)
        else:
            p.add_suggests(suggests)
    if supplements:
        if isinstance(supplements, list):
            for supplement in supplements:
                p.add_supplements(supplement)
        else:
            p.add_supplements(supplements)

    if provides:
        if isinstance(provides, list):
            for provide in provides:
                p.add_provides(provide)
        else:
            p.add_provides(provides)
    if conflicts:
        if isinstance(conflicts, list):
            for conflict in conflicts:
                p.add_conflict(conflict)
        else:
            p.add_conflicts(conflicts)
    if obsoletes:
        if isinstance(obsoletes, list):
            for obsolete in obsoletes:
                p.add_obsoletes(obsolete)
        else:
            p.add_obsoletes(obsoletes)

    return p


def create_repos(data, outdir = None):
    """
    @param data structure parsed from the yaml file
    @param outdir path in filesystem specifying destination directory
        default value None creates tempdir
    @returns tuple with three items representing paths to directories
        'current repo', 'updates repo', 'outdir'
    """

    # prepare directories
    if outdir is None:
        outdir = tempfile.mkdtemp(prefix = "depcheck_scenarios")

    cur_dir = os.path.join(outdir, 'repo_current')
    upd_dir = os.path.join(outdir, 'repo_updates')
    crpm_dir = os.path.join(outdir, 'crpms')
    urpm_dir = os.path.join(outdir, 'urpms')
    for dirname in [outdir, cur_dir, upd_dir, crpm_dir, urpm_dir]:
        if not os.path.isdir(dirname):
            os.makedirs(dirname)

    # build the rpms
    for package in data['current_repo']:
        p = package_to_SimpleRpmBuild(package, crpm_dir)
        p.make()

    for package in data['update_repo']:
        p = package_to_SimpleRpmBuild(package, urpm_dir)
        p.make()

    # crate repos
    for rpmdir, repodir in [(crpm_dir, cur_dir), (urpm_dir, upd_dir)]:
        # each directory represents one rpm
        for directory in os.listdir(rpmdir):
            # copy all "*.rpm" files from the RPMS directory (we do not want SRCRPMS)
            for root, dirs, files in os.walk(os.path.join(rpmdir, directory, 'RPMS')):
                for file in [f for f in files if f.endswith('.rpm')]:
                    subprocess.check_call(['cp', os.path.join(root, file), repodir])
                    print "copied %s to %s" % (file, repodir)
        # create repo
        subprocess.check_call(['createrepo', repodir])

    return (cur_dir, upd_dir, outdir)


if __name__ == "__main__":
    import sys
    try:
        data = yaml.load(open(sys.argv[1]).read())
    except:
        print >> sys.stderr, ("\n\nMust supply valid yaml.")
        sys.exit(1)

    create_repos(data)
