"""This is a set of functional tests based on the depcheck_scenarios repo
 - https://bitbucket.org/fedoraqa/depcheck_scenarios

The basic idea here is to create a safety net of functional tests to make sure
that nothing breaks during refactoring since we have no significant unit test
coverage at this time.

After the refactoring is done, it will serve as a useful set of functional
tests for depcheck-mk-2
"""

import os
import pytest
import yaml

import depcheck
from depcheck.depcheck import Depcheck
from depcheck import metadata
from depcheck import Pass
from .depcheck_scenarios import yaml_to_repos

import pprint

def get_depcheck_scenarios():
    """assuming that the depcheck_scenarios repo is checked out in the tests/
    directory, scan the directory structure for scenarios that should pass or
    fail.

    :returns: list of tuples of the form (should_pass, path_to_scenario_yaml)
    """
    yamlfiles = []
    for scenario_type in ['pass', 'fail']:
        scenario_path = os.path.join(os.path.dirname(__file__),
                                     'depcheck_scenarios/scenarios/%s' % scenario_type)
        dircontents = os.walk(scenario_path)
        for dircontent in dircontents:
            if len(dircontent[2]) == 0:
                continue
            if dircontent[1] == 'known_broken':
                continue
            for infile in dircontent[2]:
                filename = '%s/%s' % (dircontent[0], infile)
                if infile.split('.')[-1] in ['yml', 'yaml']:
                    yamlfiles.append((scenario_type == 'pass', filename))
    return yamlfiles


@pytest.fixture(params=get_depcheck_scenarios())
def depcheck_scenario(request):
    return request.param


def is_output_pass(output):
    result = True
    for key, value in output.items():
        result = result and (value['result'] == Pass)
    return result

def read_yamldata(filename):
    with open(filename, 'r') as filedata:
        return yaml.load(filedata)


def test_depcheck_scenarios(depcheck_scenario, tmpdir, monkeypatch):
    # decode the input tuple for readability
    scenario_pass = depcheck_scenario[0]
    scenario_path = depcheck_scenario[1]

    print "\n### Running scenario %s" % scenario_path

    # convert the yaml scenario to fake rpms and repos
    scenario_data = read_yamldata(scenario_path)
    current_repo, update_repo, dummy = \
        yaml_to_repos.create_repos(scenario_data, outdir=str(tmpdir))

    # get list of rpms
    rpm_list = depcheck.dir_to_rpms(str(update_repo))

    # handle data formatting for repo metadata (list of dicts)
    repos = {'current': {'arch': 'x86_64', 'path': str(current_repo)}}
    md_handle = metadata.DepcheckMetadata(repos,
                                          targetdir=str(tmpdir))
    repomd_files = md_handle.repodata

    # run depcheck on the scenario's repos and rpms
    depck = Depcheck('x86_64', repomd_files)
    run_result = depck.check_rpm_deps(rpm_list)

    pprint.pprint(run_result)

    assert is_output_pass(run_result) is scenario_pass
