import pytest
from dingus import Dingus
import __builtin__

from depcheck import depcheck
from depcheck import Pass, Fail
from depcheck import exc


class TestDepcheck(object):
    def test_libsolv_setup_adds_repos(self, monkeypatch):
        ref_arch = 'x86_64'
        ref_basepath = '/path/to/testing-repo/'
        ref_repomdfiles = {'reponame': {'primary': '%sprimary.xml.gz' % ref_basepath,
                                        'filelists': '%sfilielists.xml.gz' % ref_basepath}}

        stub_solvpool = Dingus('solvpool')
        stub_solvrepo = Dingus('solvrepo')
        stub_open = Dingus('open')
        test_depcheck = depcheck.Depcheck(ref_arch, ref_repomdfiles)
        test_depcheck.solvpool_object = stub_solvpool
        test_depcheck.solvrepo_object = stub_solvrepo

        monkeypatch.setattr(__builtin__, 'open', stub_open)

        test_depcheck._prepare_libsolv()

        assert len(stub_solvrepo.calls()) == 2

    def test_check_rpm_deps_output(self, monkeypatch):
        """
        Tests whether rpm_deps provides correct output for a given input.
        """

        test_depcheck = depcheck.Depcheck(None, None, "fakedir")
        monkeypatch.setattr(test_depcheck, '_prepare_libsolv', Dingus())

        rpmfiles = ['rpm_1', 'rpm_2']
        def fake_check_rpm(rpmfile, solvable):
            results = {
                    rpmfiles[0]: {'result':Pass, 'details':[]},
                    rpmfiles[1]: {'result':Fail, 'details':['message']},
                    }
            return results.get(rpmfile, (Pass, []))

        monkeypatch.setattr(test_depcheck, '_check_rpm', fake_check_rpm)

        outcome = test_depcheck.check_rpm_deps(rpmfiles)

        assert len(outcome) == 2
        assert 'rpm_1' in outcome
        assert 'rpm_2' in outcome
        assert outcome['rpm_1']['result'] == Pass
        assert outcome['rpm_2']['result'] == Fail
        assert outcome['rpm_2']['details'] == ['message']

