import depcheck


class TestDepcheckUtils(object):
    def test_repo_arg_parsing_remote(self):
        ref_repourl = 'https://host/with/repo/'
        ref_reponame = 'somerepo'
        ref_inputarg = '%s:%s' % (ref_reponame, ref_repourl)

        test_reponame, test_repourl = depcheck.extract_named_repo(ref_inputarg)

        assert test_reponame == ref_reponame
        assert test_repourl == ref_repourl

    def test_repo_arg_parsing_local(self):
        ref_repopath = '/path/to/repo'
        ref_reponame = 'somerepo'
        ref_inputarg = '%s:%s' % (ref_reponame, ref_repopath)

        test_reponame, test_repourl = depcheck.extract_named_repo(ref_inputarg)

        assert test_reponame == ref_reponame
        assert test_repourl == ref_repopath